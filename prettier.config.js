// eslint-disable-next-line no-undef
export default {
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  printWidth: 100,
  useTabs: false,
  vueIndentScriptAndStyle: false,
  bracketSpacing: true,
};
