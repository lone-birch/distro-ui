import { Directive } from 'vue';

// Add IE-specific interfaces to Window
type IEWindow = Window &
  typeof window & {
    attachEvent: (event: string, listener: EventListener) => boolean;
    detachEvent: (event: string, listener: EventListener) => void;
  };

// Add IE-specific interfaces to Element
type IEHTMLTextAreaElement = HTMLTextAreaElement & {
  attachEvent: (event: string, listener: EventListener) => boolean;
  detachEvent: (event: string, listener: EventListener) => void;
};

const vAutoGrow: Directive = {
  mounted(el: HTMLTextAreaElement) {
    let observe: (element: IEHTMLTextAreaElement, event: string, handler: EventListener) => void;
    let minHeight = 0;

    // If used in a component library such as buefy, a wrapper will be used on the component so check if a child is the textarea
    el = el.tagName.toLowerCase() === 'textarea' ? el : el.querySelector('textarea')!;

    minHeight = parseFloat(getComputedStyle(el).getPropertyValue('min-height'));

    if ((<IEWindow>window).attachEvent) {
      observe = function (element, event, handler) {
        element.attachEvent('on' + event, handler);
      };
    } else {
      observe = function (element, event, handler) {
        element.addEventListener(event, handler, false);
      };
    }

    const resize = () => {
      el.style.height = 'auto';
      const border = Number(el.style.borderTopWidth) * 2;
      el.style.setProperty(
        'height',
        (el.scrollHeight < minHeight ? minHeight : el.scrollHeight) + border + 'px',
        'important',
      );
    };

    // 0-timeout to get the already changed el
    const delayedResize = () => {
      window.setTimeout(resize, 0);
    };

    // When the textarea is being shown / hidden
    const respondToVisibility = (element: IEHTMLTextAreaElement, callback: () => void) => {
      const intersectionObserver = new IntersectionObserver(
        (entries) => {
          entries.forEach((entry) => {
            if (entry.intersectionRatio > 0) callback();
          });
        },
        {
          root: document.documentElement,
        },
      );

      intersectionObserver.observe(element);
    };

    const element = el as IEHTMLTextAreaElement;
    respondToVisibility(element, resize);
    observe(element, 'change', resize);
    observe(element, 'cut', delayedResize);
    observe(element, 'paste', delayedResize);
    observe(element, 'drop', delayedResize);
    observe(element, 'input', resize);

    resize();
  },
};

export default vAutoGrow;
