/* eslint-env node */
/* eslint-disable @typescript-eslint/no-var-requires */

const fs = require('fs-extra');
const path = require('path');
const glob = require('glob');

const source = path.join(__dirname, 'dist', 'src');
const destination = path.join(__dirname, 'dist');

// Check if `dist/src` exists before attempting to copy
if (fs.existsSync(source)) {
  fs.copySync(source, destination);
  fs.removeSync(source);
}

// Get all .mjs files in the dist folder
const files = glob.sync('dist/**/*.mjs');

files.forEach((file) => {
  let content = fs.readFileSync(file, 'utf-8');

  // Simplify replacements for _virtual and node_modules paths
  content = content.replace(/(\.\.\/)+_virtual\//g, '../_virtual/');
  content = content.replace(/(\.\.\/)+node_modules\//g, '../node_modules/');

  fs.writeFileSync(file, content);
});